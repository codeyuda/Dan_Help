package com.example.asus.androidpi.Interface;

import android.view.View;

/**
 * Created by Asus on 26/06/2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
