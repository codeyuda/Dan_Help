package com.example.asus.androidpi;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.androidpi.Common.Common;
import com.example.asus.androidpi.Model.Proyektor;
import com.example.asus.androidpi.Model.User;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    // creating arraylist of Proyektor() type to set to adapter

    FirebaseDatabase database;
    DatabaseReference proyektor;

    FirebaseRecyclerAdapter adapter;


    TextView txtFullName;
    RecyclerView recycler_menu;
    RecyclerView.LayoutManager layoutManager;

    Integer jumlah = 50;

    Button btnPinjam, btnKembalikan;
    CheckBox checkBox_header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //init Firebase
        database = FirebaseDatabase.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Proyektor");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //set Name
        View headerView = navigationView.getHeaderView(0);
        txtFullName = (TextView) headerView.findViewById(R.id.txtFullName);
        txtFullName.setText(Common.currentUser.getName());


        btnPinjam = findViewById(R.id.btn_pinjam);
        btnPinjam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
                android.support.v4.app.DialogFragment dialogFragment = new com.example.asus.androidpi.DialogFragment();
                dialogFragment.show(manager,"MessageDialog");

                Log.i("TAG","Just Show it");
            }
        });
        btnKembalikan = findViewById(R.id.btn_kembalikan);
        btnKembalikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home.this, ListPeminjam.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent a = new Intent(this, Home.class);
            startActivity(a);
            finish();
        } else if (id == R.id.nav_list_proyektor) {
            Intent list = new Intent(this, ListPeminjam.class);
            startActivity(list);
        } else if (id == R.id.nav_waiting_list) {
            Intent wl = new Intent(this, WaitingList.class);
            startActivity(wl);
        } else if (id == R.id.nav_sign_out) {
            Intent signout = new Intent(this, MainActivity.class);
            signout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(signout);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Pinjam Proyektor");
        alertDialog.setMessage("Please isi semua data");
        //Adding data i.e images and title to be set to adapter to populate listview
        //here i am passing  string to be set as title and boolean as a parameter to Proyektor() Constructor as our
        //ArrayList is type of Proyektor()

        LayoutInflater inflater = this.getLayoutInflater();
        final View update_pinjam_layout = inflater.inflate(R.layout.pinjam_item, null);

        final ListView listViewWithCheckbox = update_pinjam_layout.findViewById(R.id.list_view_with_checkbox);

        // Initiate listview data.
        final List<ListViewItemDTO> initItemList = this.getInitViewItemDtoList();

        // Create a custom list view adapter with checkbox control.
        final ListViewItemCheckboxBaseAdapter listViewDataAdapter = new ListViewItemCheckboxBaseAdapter(this, initItemList);

        listViewDataAdapter.notifyDataSetChanged();
        // Set data adapter to list view.
        listViewWithCheckbox.setAdapter(listViewDataAdapter);

        // When list view item is clicked.
        listViewWithCheckbox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long l) {
                // Get user selected item.
                Object itemObject = adapterView.getAdapter().getItem(itemIndex);


                // Translate the selected item to DTO object.
                ListViewItemDTO itemDto = (ListViewItemDTO) itemObject;
                Toast.makeText(getApplicationContext(), "select item text : " + itemDto.getItemText(), Toast.LENGTH_SHORT).show();


                // Get the checkbox.
                CheckBox itemCheckbox = (CheckBox) view.findViewById(R.id.list_view_item_checkbox);

                // Reverse the checkbox and clicked item check state.
                if (itemDto.isChecked()) {
                    itemCheckbox.setChecked(false);
                    itemDto.setChecked(false);
                } else {
                    itemCheckbox.setChecked(true);
                    itemDto.setChecked(true);
                }

            }
        });

        alertDialog.setView(update_pinjam_layout);
        alertDialog.setPositiveButton("pinjam", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final EditText edtNama, edtKelas, edtProyektor;
                String jam = "";
                edtNama = update_pinjam_layout.findViewById(R.id.edtNama);
                edtKelas = update_pinjam_layout.findViewById(R.id.edtKelas);
                edtProyektor = update_pinjam_layout.findViewById(R.id.edtProyektor);

//                for (int o = 0; o < CostumAdapter.modelArrayList.size(); o++) {
//                    if (CostumAdapter.modelArrayList.get(o).getSelected()) {
//                        Log.e("CheckBox Selected : ", String.valueOf(o));
////                        item.setJam("Jam "+ o);
//                        jam = jam + o + ", ";
//
//
//                    }
//                }
                String nama = edtNama.getText().toString();
                String kelas = edtKelas.getText().toString();
                String noProyektor = edtProyektor.getText().toString();


                Proyektor p = new Proyektor();
                p.setNama(nama);
                p.setKelas(kelas);
                p.setNoProyektor(noProyektor);


//                item.setNama(edtNama.getText().toString());
//                item.setKelas(edtKelas.getText().toString());
//                item.setStatus("Unavailable") ;

                String dir = "Kampus/" + Common.currentUser.getName() + "/Dipakai";
                DatabaseReference usersRef = database.getReference(dir);
//                usersRef.child("jumlah").setValue(jumlah);
                usersRef.child(noProyektor).setValue(new Proyektor(nama, kelas));

//                proyektor.setValue(item);
//                Proyektor p = new Proyektor(true,jam,nama,edtKelas.getText().toString(),"unavailasble");
//                proyektor.child("Nama :").setValue(nama);
//                proyektor.child("Kelas :").setValue(kelas);

                dialogInterface.dismiss();
                Toast.makeText(Home.this, "Silahkan Memakai Proyektornya", Toast.LENGTH_SHORT).show();
//                int foo = Integer.parseInt(noProyektor);
//                Log.e("sebelum: ",String.valueOf(numbers));
//                numbers.add(foo);
//                Log.e("HAI YUDA: ",String.valueOf(numbers));

            }
        });
        alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(Home.this, "hai yuda", Toast.LENGTH_SHORT).show();
            }
        });


        alertDialog.show();
    }

    public static List<ListViewItemDTO> getInitViewItemDtoList() {
        String itemTextArr[] = {"Android", "iOS", "Java", "JavaScript", "JDBC", "JSP", "Linux", "Python", "Servlet", "Windows"};

        List<ListViewItemDTO> ret = new ArrayList<>();

        int length = itemTextArr.length;
        Log.e("getInitViewItemDtoList", Arrays.toString(itemTextArr));
        for (int i = 0; i < length; i++) {
            String itemText = itemTextArr[i];
            Log.e("getInitViewItemDtoList ", itemTextArr[0]);

            ListViewItemDTO dto = new ListViewItemDTO();
            dto.setChecked(false);
            dto.setItemText(itemText);

            ret.add(dto);
        }

        return ret;

    }
}
