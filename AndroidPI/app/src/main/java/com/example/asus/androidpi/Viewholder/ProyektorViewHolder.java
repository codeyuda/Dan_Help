package com.example.asus.androidpi.Viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.asus.androidpi.Common.Common;
import com.example.asus.androidpi.Interface.ItemClickListener;
import com.example.asus.androidpi.R;

/**
 * Created by Asus on 26/06/2018.
 */

public class ProyektorViewHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener,
        View.OnCreateContextMenuListener{

    public TextView noProyektor;
    public TextView nama;
    public TextView kelas;
    public TextView jam;
    public TextView jamPinjam;
    public Button btnKembali;

    private ItemClickListener itemClickListener;

    public ProyektorViewHolder(View itemView) {
        super(itemView);
        noProyektor = itemView.findViewById(R.id.tvProyektor);
        nama = itemView.findViewById(R.id.tvNama);
        kelas = itemView.findViewById(R.id.tvKelas);
        jam = itemView.findViewById(R.id.tvJam);
        jamPinjam = itemView.findViewById(R.id.tvJamPinjam);
        btnKembali = itemView.findViewById(R.id.btnKembali);

        itemView.setOnCreateContextMenuListener(this);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.setHeaderTitle("Select the Action");

        contextMenu.add(0,0,getAdapterPosition(), Common.UPDATE);
        contextMenu.add(0,1,getAdapterPosition(),Common.DELETE);


    }
}
