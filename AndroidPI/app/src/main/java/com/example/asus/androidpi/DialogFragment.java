package com.example.asus.androidpi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Asus on 29/07/2018.
 */

public class DialogFragment extends AppCompatDialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //create the view to show
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.pinjam_item, null);

        final ListView listViewWithCheckbox = v.findViewById(R.id.list_view_with_checkbox);

        final List<ListViewItemDTO> initItemList = Home.getInitViewItemDtoList();
        final ListViewItemCheckboxBaseAdapter listViewDataAdapter = new ListViewItemCheckboxBaseAdapter(getContext(), initItemList);

        listViewDataAdapter.notifyDataSetChanged();
        // Set data adapter to list view.
        listViewWithCheckbox.setAdapter(listViewDataAdapter);
        listViewWithCheckbox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long l) {
                // Get user selected item.
                Object itemObject = adapterView.getAdapter().getItem(itemIndex);


                // Translate the selected item to DTO object.
                ListViewItemDTO itemDto = (ListViewItemDTO) itemObject;
                Toast.makeText(getContext(), "select item text : " + itemDto.getItemText(), Toast.LENGTH_SHORT).show();


                // Get the checkbox.
                CheckBox itemCheckbox = (CheckBox) view.findViewById(R.id.list_view_item_checkbox);

                // Reverse the checkbox and clicked item check state.
                if (itemDto.isChecked()) {
                    itemCheckbox.setChecked(false);
                    itemDto.setChecked(false);
                } else {
                    itemCheckbox.setChecked(true);
                    itemDto.setChecked(true);
                }

            }
        });
        //create button listener
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("TAG", "Clicked");
            }
        };
        return new AlertDialog.Builder(getActivity())
                .setTitle("TITLE")
                .setView(v)
                .setPositiveButton("OK", listener)
                .create();
    }
}
