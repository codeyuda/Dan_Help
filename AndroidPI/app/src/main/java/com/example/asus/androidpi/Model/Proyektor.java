package com.example.asus.androidpi.Model;

/**
 * Created by Asus on 22/06/2018.
 */

public class Proyektor {
    private String nama;
    private String kelas;
    private String status;
    private String noProyektor;
    private Integer jumlah;
    private String title="";
    private String name;
    private boolean selected;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    public Proyektor() {
    }


    public Proyektor( String Nama, String Kelas) {

        nama = Nama;
        kelas = Kelas;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public String getNoProyektor() {
        return noProyektor;
    }

    public void setNoProyektor(String noProyektor) {
        this.noProyektor = noProyektor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
