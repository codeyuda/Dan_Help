package com.example.asus.androidpi;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.asus.androidpi.Common.Common;
import com.example.asus.androidpi.Interface.ItemClickListener;
import com.example.asus.androidpi.Model.Proyektor;
import com.example.asus.androidpi.Model.User;
import com.example.asus.androidpi.Viewholder.ProyektorViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ListPeminjam extends AppCompatActivity {

    EditText edtNama, edtKelas, edtProyektor;

    FirebaseDatabase database;
    DatabaseReference proyektor;
    RecyclerView recycler_menuu;
    FirebaseRecyclerAdapter<Proyektor, ProyektorViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_peminjam2);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        String dir = "Kampus/" + Common.currentUser.getName()+"/Dipakai";
        proyektor = database.getReference(dir);
        RecyclerView.LayoutManager layoutManager;

        recycler_menuu = findViewById(R.id.recycler_menu);
        recycler_menuu.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recycler_menuu.setLayoutManager(layoutManager);
        loadmenu();

    }

    private void loadmenu() {
        adapter = new FirebaseRecyclerAdapter<Proyektor, ProyektorViewHolder>(Proyektor.class, R.layout.model_item, ProyektorViewHolder.class, proyektor) {
            @Override
            protected void populateViewHolder(ProyektorViewHolder viewHolder, final Proyektor model, int position) {
                viewHolder.nama.setText(model.getNama());
                viewHolder.kelas.setText(model.getKelas());
                viewHolder.noProyektor.setText(getRef(position).getKey());

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Toast.makeText(ListPeminjam.this, "you clicked " + model.getNama(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        recycler_menuu.setAdapter(adapter);

    }

    //ContextMenu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.UPDATE)) {
            showUpdate(adapter.getRef(item.getOrder()).getKey());
            Log.e("onContextItemSelected: ", adapter.getRef(item.getOrder()).getKey());
        }
        if (item.getTitle().equals((Common.DELETE))) {
            deleteItem(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);
    }

    private void deleteItem(String key) {
        proyektor.child(key).removeValue();
    }

    private void showUpdate(final String key) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ListPeminjam.this);
        alertDialog.setTitle("Pinjam Proyektor");
        alertDialog.setMessage("Please isi semua data");

        LayoutInflater inflater = this.getLayoutInflater();
        final View update_pinjam_layout = inflater.inflate(R.layout.pinjam_item, null);



        alertDialog.setView(update_pinjam_layout);
        alertDialog.setNeutralButton("Kembalikan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.setPositiveButton("pinjam", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final EditText edtNama, edtKelas, edtProyektor;
                String jam = "";
                edtNama = update_pinjam_layout.findViewById(R.id.edtNama);
                edtKelas = update_pinjam_layout.findViewById(R.id.edtKelas);
                edtProyektor = update_pinjam_layout.findViewById(R.id.edtProyektor);

                String nama = edtNama.getText().toString();
                String kelas = edtKelas.getText().toString();
                String noProyektor = edtProyektor.getText().toString();


                Proyektor p = new Proyektor();
                p.setNama(nama);
                p.setKelas(kelas);
                p.setNoProyektor(noProyektor);


//                item.setNama(edtNama.getText().toString());
//                item.setKelas(edtKelas.getText().toString());
//                item.setStatus("Unavailable") ;

                String dir = "Kampus/" + Common.currentUser.getName()+"/Dipakai";
                DatabaseReference usersRef = database.getReference(dir);
//                usersRef.child("jumlah").setValue(jumlah);
                usersRef.child(key).setValue(new Proyektor( nama, kelas));

//                proyektor.setValue(item);
//                Proyektor p = new Proyektor(true,jam,nama,edtKelas.getText().toString(),"unavailasble");
//                proyektor.child("Nama :").setValue(nama);
//                proyektor.child("Kelas :").setValue(kelas);

                dialogInterface.dismiss();
                Toast.makeText(ListPeminjam.this, "Silahkan Memakai Proyektornya", Toast.LENGTH_SHORT).show();
//                int foo = Integer.parseInt(noProyektor);
//                Log.e("sebelum: ",String.valueOf(numbers));
//                numbers.add(foo);
//                Log.e("HAI YUDA: ",String.valueOf(numbers));

            }
        });
        alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });


        alertDialog.show();
    }


}
